# KeyDuino
A C program to make the Arduino board download a virus (or other program) and execute it.

# Compatibility

This program is compatible with the Arduino/Genuino:
Uno (HoodLoader2 required)
Mega (HoodLoader2 required)
Leonardo
Micro

# Prerequisites

- To be able to compile this software you need the HID-Project arduino library. (https://github.com/NicoHood/HID)

- If you have an Arduino Uno / Mega, install HoodLoader2 first (https://github.com/NicoHood/HoodLoader2)
